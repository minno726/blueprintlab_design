## Factorio Mod The Blueprint lab
Adds a way to enter a blueprints lab so you have a testbech for your blueprints.

This is only an update from the original mod to make it work with latest vesions of +0.18.27 and make it more polished.

_If I can, I will add new features like the new shortcut buttons or a reaserch to enable it (because I like to research things)._

This is my second or third mod and I dont know if I will have the necesary skills to keep it updated.